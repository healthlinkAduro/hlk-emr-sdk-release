## Introduction

HLK EMR SDK release for customers with documentation

## Deployment Architecture and Infrastructure setup

[External architecture](docs/figures/dev-deployment.png)


## Form EMR workflow design

- Form pre-population from EMR workflow is [here](docs/figures/form-prepopulation.png)
- Form parking to EMR workflow is [here](docs/figures/form-parking.png)
- Form un-parking to EMR workflow is [here](docs/figures/form-unparking.png)
- Form submission and writeback workflow to EMR is [here](docs/figures/form-submission.png) 

## Tutorials and supporting documentation

- EMR tutorials [here](docs/tutorials/emr)
- HL EMR integration [here](docs/tutorials/hl-emr-integration)

### Prerequisites

Node: v10.15.3

NPM: 6.10.2 

### Installing

Copy the package to your project that will use the EMR SDK API and then install using the below: 

`npm install hlk-emr-sdk-0.5.2.tgz`




## Interface overview 

The API has the following public interface methods supporting the following functionalities:

1. Form Prepopulation
2. Form Parking.
3. Form Unparking.
4. Form Submission.
5. Fetch Attachment data.
6. Get Version.
7. Get Form View.
8. Process Action.
9. Get Delivery Options.
10. Send Submission Details

## Interface definitions

**Form Prepopulation/Unparking**

` prePopulateForm(sessionKey: string, emrUrl: string, emrFormMetaDataXml:
        any, dynamicTemplateRequestXml: string, isUnparked: boolean);`

| Parameter   | Description  | 
|---|---|
|sessionKey   | Required for EMR interaction, is obtained via URL query parameter  |
|emrUrl   |EMR call back URL   |
|emrFormMetaDataXml   |JavaScript Object (see object structure below)   |
|dynamicTemplateRequestXml|Template XML|
|isUnparked|Indicates if the prepopulation is for a new form. If a parked form needs to be prepopulated, then set this value to true.|

Response (with ServerResponse object, see Response Parsing section below):
JSON Response that is a representation of the Aduro XML.
(see [JSON - Aduro XML Data Structure](docs/tutorials/hl-emr-integration/2019-07-30-JSON-Aduro-Data-Structures.docx))

**Form Parking**

`parkForm(sessionKey: string, emrUrl: string, formMetadata: any, formJSON: string,
              formResumePath: string, printableview: string, viewType: string, completed: boolean , continueSession);`
              
| Parameter   | Description  | 
|---|---|
|sessionKey   | Required for EMR interaction, is obtained via URL query parameter  |
|emrUrl   |EMR call back URL   |
|formMetadata   |JavaScript Object (see object structure below)   |
|formJSON| JSON Object returned by the EMR API that will be transformed back to Aduro XML |
|formResumePath|The path encoded in Base64, that Forms Director will decode to resume a parked form, see example below.|
|printableview|The printable view as escaped HTML.|
|viewType|Indicates the mime type of the printable view typically `text/html`|
|completed|Indicates if the session is complete, should be false for parking a new form.|
|continueSession|Indicates if the session will be continued later.|              
 
 Example FormResumePath:
 
 `/forms-directory/LaunchForm.action?aduro_formDefinitionId=forms-directory(ampersand)' +
             'resumePathParams=L2hlYWx0aGxpbmsvM3JkUGFydHlmb3Jtcy9lbXJzZGtkZW1vZm9ybS9pbmRleC5odG1sP2FkdXJvX2Zvcm1EZWZpbml0aW9uSWQ9a29ubmVjdF9kZW1v'`
 
 Where resumePathParams value is Base64 Encoded text, which in this case is:
 
 `/healthlink/3rdPartyforms/emrsdkdemoform/index.html?aduro_formDefinitionId=konnect_demo`
 
Response (with ServerResponse object, see Response Parsing section below).
 
**Form Submission**

`submit(sessionKey: string, emrUrl: string, formMetadata: any, submissionMetadata: any, formJSON: string,
            recipientOrgEDI: string, recipientMessagingAddress: string);`
            
| Parameter   | Description  | 
|---|---|
|sessionKey   | Required for EMR interaction, is obtained via URL query parameter  |
|emrUrl   |EMR call back URL   |
|formMetadata   |JavaScript Object (see object structure below)   |
|submissionMetadata| JavaScript object, contains the metadata that will be used for Submitting through Forms Director, see structure below. |
|formJSON|The path encoded in Base64, that Forms Director will decode to resume a parked form, see example below.|
|recipientOrgEDI|The recipient organisation EDI.|
|recipientMessagingAddress|The recipient messaging address.| 

Response (with ServerResponse object, see Response Parsing section below).

**Fetch Attachment Data**

`fetchAttachmentData(sessionKey: string, emrUrl: string, formMetadata: any, attachmentRequest: any);`

| Parameter   | Description  | 
|---|---|
|sessionKey   | Required for EMR interaction, is obtained via URL query parameter  |
|emrUrl   |EMR call back URL   |
|emrFormMetaData   |JavaScript Object (see object structure below)   |
|attachmentRequest| JavaScript object or an array of objects (for multiple attachments), specifying the section, group details of the required attachment. |


Response (with ServerResponse object, see Response Parsing section below):

JSON Response that is a representation of the Aduro XML.
(see [JSON - Aduro XML Data Structure](docs/tutorials/hl-emr-integration/2019-07-30-JSON-Aduro-Data-Structures.docx))

***Object Parameters***

**FormMetadata**

JavaScript Object for the API call:

`formMetadata = {
              formEngineId: '',
              formInstanceXmlDate: '',
              formInstanceOperationMode: 'N',
              formDefinitionId: 'konnect_demo',
              formDefinitionVersion: '1.0',
              formDefinitionDescription: 'HealthLink eReferral Form for Heartbeat Health Summary Form',
              formDefinitionTitle: 'HealthLink eReferral Form for Heartbeat Health Summary Form',
              formInstanceDescription: 'HealthLink eReferral Form for Heartbeat Health Summary Form',
              formInstanceVersion: '1.0',
              formInstanceId: 'KN-00001',
              recipient: 'konnet23',
          };`

 
 FormMetadata Attributes Source 
 
 Note: 
 1. Some attributes can be mapped from the Form invocation URL (see sample below the table). 
 2. Some attribute values will be configured by HealthLink.
 3. Other attribute values are simple descriptors, check with HealthLink for additional info.
  
 |  FormMetadata Attribute   |  Source|  
 |---|---|
 |formEngineId   | Not used for Aduro. Leave it blank. |
 |formInstanceXmlDate   |  EMR SDK API will use current date if not supplied |
 |formDefinitionId   | (URL) aduro_formDefinitionId. The value will have to be configured by HealthLink to appear in the URL. Please check with HealthLink. |
 |formInstanceOperationMode   | (URL) aduro_mode |
 |formDefinitionVersion  | Check with HealthLink   |
 |formDefinitionDescription  | Check with HealthLink   |
 |formInstanceDescription  | Check with HealthLink   |
 |formInstanceId| Unique ID generated per Submission - Check with HealthLink for specification. |
 |recipient| Check with HealthLink|
 
 
 Sample Third-party Form URL:

`http://localhost:4200/index.html?aduro_formDefinitionId=konnect_demo&emr_displayBrowser=internal&messagingAddress=yazdvedi&aduro_method=new&aduro_mode=N&aduro_aduroVersion=1.1&aduro_sessionKey=7537e432-47c6-4adc-a5d8-2945ada8b930&aduro_formDefinitionVersion=1.0&aduro_callbackURL=http:%2F%2FAUBP698:8080%2Fhealthlink%2F%3FWSDL&aduro_dictionaryVersion=1.0&aduro_resumePath=$`




**Attachment Request Object**

Example Attachment Request Object Structure

`attachmentRequest = {
	sectionName:'scannedDocuments',
	groupName: 'Patient_Attachment',
	name: 'scannedDocument',
	conceptID: '554:1000111/1000455',
	referenceID: '79'
};`

Reference to Aduro XML

`<section name="scannedDocuments">
                            <group conceptID="554:1000111/1000455" conceptName="Patient_Attachment" name="scannedDocument" referenceID="29">
                                <field conceptID="554:1000111/1000455/4" conceptName="Patient_Attachment_Content" name="content"/>
                            </group>
                        </section>`



**Submission Request Object**

`submissionRequest = {
             sendApplication: 'Best Practice 1.10.0.875',
             senderAccount: 'yazdvedi',
             senderPassword: '',
             messageId: 'KO-00001',
             submissionPath: '/form-au-route/soapListener'
         };`

 |  Attribute   |  Source|  
 |---|---|
 |sendApplication   | Use response from getVersion call (application + application version) |
 |senderAccount   | Third-party EDI (can be obtained from the URL param messagingAddress )  |
 |senderPassword   |Check with HealthLink  |
 |messageId   | Same as formInstanceId in Form Metadata object |
 |submissionPath  | Check with HealthLink   |

**Get Version**

` getVersion(sessionKey: string, emrUrl: string);`

| Parameter   | Description  | 
|---|---|
|sessionKey   | Required for EMR interaction, is obtained via URL query parameter  |
|emrUrl   |EMR call back URL   |

Response (with ServerResponse object, see Response Parsing section below):

`{"application":"Best Practice","applicationVersion":"1.10.0.875","interfaceVersion":"1.1","dictionaryVersion":"3.0"}`

**Get Delivery Options**

` getDeliveryOptions(sessionKey: string, emrUrl: string);`

| Parameter   | Description  | 
|---|---|
|sessionKey   | Required for EMR interaction, is obtained via URL query parameter  |
|emrUrl   |EMR call back URL   |

Response (with ServerResponse object, see Response Parsing section below):

`{"url":"http://localhost:5000/SubmissionGateway","messageID":"20100228132009","recipientAccount":"accclaim","senderAccount":"ma65test","senderPassword":"1"}`

**Process Action**

`processAction(sessionKey: string, emrURL: string, actionID: string, processActionRequest: ProcessActionRequest)`

| Parameter   | Description  | 
|---|---|
|sessionKey   | Required for EMR interaction, is obtained via URL query parameter  |
|actionID   |See Aduro Interface Spec.   |
|processActionRequest   |See ProcessAction object below.   |

` let processActionObject = {
         code: '',
         dueDate: '',
         taskDescription: '',
         recurrenceInterval: '',
         intervalUnit: '',
         complete: '',
     };`

 |  Attribute   |  Source|  
 |---|---|
 |code   | See Aduro Interface Spec. |
 |dueDate   | See Aduro Interface Spec. |
 |taskDescription   |See Aduro Interface Spec. |
 |recurrenceInterval   | See Aduro Interface Spec. |
 |intervalUnit  | See Aduro Interface Spec.  |
 |complete  | See Aduro Interface Spec.  |

**Get Form View**

` getFormView(sessionKey: string, formInstanceID: string,  emrURL: string) `

| Parameter   | Description  | 
|---|---|
|sessionKey   | Required for EMR interaction, is obtained via URL query parameter  |
|formInstanceID   | See Aduro Interface Spec.  |
|emrUrl   |EMR call back URL   |


## Response Parsing
All above APIs return a RxJS Observable Object that is a JavaScript object with the following structure:

`Server Response ={
 code: ResponseCode;
 details: string;
 }`

The `code` will contain either `OK` `or` ERROR depending on the response.
The `details` attribute contains the JSON string when the response is successful or error details 
when the response has errors.

**Send Submission Details**

` sendSubmissionDetails(submissionDetails: SubmissionDetails) `

| Parameter   | Description  | 
|---|---|
|submissionDateTime|The timestamp the Form was submitted in format: YYYY-MM-DDTHH:MM:SSZ |
|sender|The EDI of the Sender|
|recipient|The EDI of the Receiver|
|messageID|The Message ID or the Business Transaction ID that uniquely identifies the Form (User friendly ID)|
|messageType|This is an identifier supplied to the third-party for the purposes of logging.|
|appType|For Aduro Form the value should be `ADUROFORMS`|
|sendingApplication|The application that is submitting the Form (typically an EMR Vendor name with the version of the instance)|
|submissionStatus|The status of the submission, applicable values: `<<Blank>>` for Submission Request, `AA` for Successful Submission or `AE` for Error in Submission|
|processingID| Applicable values: `T` - a test Form was submitted, `D`- form submitted for debugging purposes, `P` - a production Form.|
|fileSize| Reserved for Future Use. Empty or null acceptable| | 
|sessionID|The Aduro Session ID|
|filePath|Reserved for future use. Empty or null acceptable| | 
|isResponseMessage|Indicates if the record is for a response of a Submission. Acceptable values: `Y` - Indicates if it's a Submission response record.`N` - Indicates if it's a Submission request record. | 
|isReplyToMessage|Reserved for future use. Empty or null acceptable| 
|isReplyToLog|Reserved for future use. Empty or null acceptable| 
|recipientAlias|EDI or Alias ID of the receiver| 
|senderAlias|EDI or Alias ID of the sender| 

## Versioning
We use [SemVer](http://semver.org/) for versioning.

## Release Notes
**0.5.2**

**Features**

* New API for sending form submission details to HealthLink backend services. 

**Bugs**

* None.

**0.5.1**

**Features**

* No change. Minor version bump.

**Bugs**

* None.

**0.5.0**

**Features**

* None.

**Bugs**

* Fixed issue with GetDeliveryOptions call when EMR did not return all values.

**0.4.0**

**Features**

* EMR API supports fetching multiple attachments using a single method.

**Bugs**

* None.


**0.3.0**

**Features**

* EMR API Logging enhancement with Session ID and additional metadata.
* Additional interface methods - Get Delivery Options, Get Form View and Process Action (to be tested with EMR).

**Bugs**

* None.

**0.2.0**

**Features**

* EMR API now logs method API entry/exits/errors via Forms Director to HealthLink.
* EMR API initialisation change. Use EDI to initialise object. Ex. `const api = new emrBridge.EmrBridge('edi');`

**Bugs**

* Outgoing HTTP calls made asynchronous. 

**0.1.0**

* Initial Release

## Issues and support

For issues and questions, contact:
